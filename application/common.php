<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**
 * @param $code string 返回值编号，如0，1，-1等
 * @param $data array 返回值
 * @param $msg string 返回描述信息
 */
function ajaxReturn ($code,$data,$msg){
    $return = array();
    $return["code"] = $code;
    $return["msg"] = $msg;
    $return['data'] = $data;
    echo json_encode($return);
    exit;
}

//ajax请求错误处理
function ajaxError($msg){
    $return = array();
    $return['code'] = 0;
    $return['msg'] = $msg;
    echo json_encode($return);
    exit;
}

function ajaxSuccess($msg){
    $return = array();
    $return['code'] =1;
    $return['msg'] = $msg;
    echo json_encode($return);
    exit;
}

/**
 * @param $msg string 日志内容
 * @param $level string 日志级别
 */
function log_record($msg, $level){
    $tmp = explode('/', Request()->pathinfo());
    $CONTROLLER_NAME = Request()->controller();
    $ACTION_NAME = Request()->action();
    \think\facade\Log::record('['.date('Y-m-d H:i:s').']['.$CONTROLLER_NAME.']['.$ACTION_NAME .']'.$msg, $level);
}

/**
 * @param $url string 发送get请求的url
 * @param $param array 请求参数
 * @param $options array 配置信息,如超时时间
 * @param $myTrim boolean 是否需要去除空格
 * @return array
 */
function c_get($url, $param, array $options = array(), $myTrim = true){
    $defaults = array(CURLOPT_HEADER => 0,
        CURLOPT_FRESH_CONNECT => 1,
        CURLOPT_RETURNTRANSFER => 1, CURLOPT_FORBID_REUSE => 1,
        CURLOPT_TIMEOUT => 3, CURLOPT_CONNECTTIMEOUT => 3,
        CURLOPT_USERAGENT => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
    if(!empty($param)){
        $fields_string = http_build_query($param);
        $url = $url.'?'.$fields_string;
    }
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL,$url) ;
    curl_setopt_array($ch, ($options + $defaults));
    $time = time();
    $date = date('Y-m-d H:i:s', $time);
    log_record($date, 'NOTIC');
    $re = curl_exec($ch);
    $time = time();
    $date = date('Y-m-d H:i:s', $time);
    log_record($date, 'notice');
    curl_close($ch);
    if($myTrim){
        $re = trim($re);
    }
    $result_decode = json_decode($re, true);
    $msg ="[url:".$url."][param:".json_encode($param)."][re:".$re."]";
    log_record($msg, 'notice');
    return $result_decode;
}

/**
 * @param $url string post请求路径
 * @param $param array 请求参数
 * @param $options array 配置信息
 * @param $json boolean 是否是json请求
 * @return array
 */
function c_post($url, array $param = NULL, array $options = array(), $json = false) {
    $defaults = array(CURLOPT_POST => 1, CURLOPT_HEADER => 0,
        CURLOPT_URL => $url, CURLOPT_FRESH_CONNECT => 1,
        CURLOPT_RETURNTRANSFER => 1, CURLOPT_FORBID_REUSE => 1,
        CURLOPT_TIMEOUT => 3, CURLOPT_CONNECTTIMEOUT => 3,
        CURLOPT_USERAGENT => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
    if($json){
        $postStr = json_encode($param);
        $defaults[CURLOPT_POSTFIELDS] = $postStr;
        $defaults[CURLOPT_HTTPHEADER] = array(
            'Content-Type: application/json',
            'Content-Length: '.strlen($postStr)
        );
    }else{
        $defaults[CURLOPT_POSTFIELDS] = http_build_query($param);
    }
    if(isset($defaults[CURLOPT_HTTPHEADER]) && isset($options[CURLOPT_HTTPHEADER])){
        $options[CURLOPT_HTTPHEADER] = array_merge($options[CURLOPT_HTTPHEADER], $defaults[CURLOPT_HTTPHEADER]);
    }

    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    G('postStartTime');
    $result = curl_exec($ch);
    G('postEndTime');
    curl_close($ch);
    $result_decode = json_decode($result, true);
    $msg ="[url:".$url."][param:".json_encode($param)."][re:".$result."][ RequestTime:".G('getStartTime','getEndTime',6)."s ]";
    log_record($msg, 'notice');
    return $result_decode;
}