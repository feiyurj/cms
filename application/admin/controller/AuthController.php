<?php
namespace app\admin\controller;


use think\Controller;

use think\facade\Config;

use think\facade\Cookie;

/**
 * 权限验证controller
 */
class AuthController extends Controller
{
    protected $userName;
    protected $userCode;
    protected $companyCode;

    protected $appCode;
    protected $model;
    protected $roleCodes = array();
    protected $orgCodes = array();

    
    protected $authService ;
    protected $authApiService ;

    //前置操作
    public function initialize()
    {
        $this->checkLogin();
        $this->checkAuth();
    }

    //登录验证(单点登录)
    private function checkLogin(){
        $ticket = $this->request->param('ticket');
        if(empty($ticket)){
            $ticket = cookie('sso_ticket');
        }
        $ssoConfig = config('SSO');
        $logicUrl = $ssoConfig['SSO_URL'].'/login?url='.$ssoConfig['THIS_URL'].'&app='.$ssoConfig['APP'];
        if(empty($ticket)){
            $url = $logicUrl;
            $this->redirect($url);
        }else{
            cookie('sso_ticket', $ticket);
        }
        //ajax 验证私钥
        $time = time();
        $tokenStr = $ssoConfig['TOKEN'] .$time.$ticket;

        $param = array();

        $param['app'] = $ssoConfig['APP'];
        $param['url'] = $ssoConfig['THIS_URL'];//app应用地址
        $param['ip'] = $_SERVER['SERVER_ADDR'];//获取服务器IP$ssoConfig['APP']
        $param['time'] = $time;

        $param['ticket'] = $ticket;
        $param['sign'] = md5($tokenStr);

        $ssoUrl = $ssoConfig['SSO_URL'].'/ajax_verify_token';
        $reqData = c_get($ssoUrl, $param);
        if($reqData['code'] !=1){
            $url = $logicUrl;
            $this->redirect($url);
        }
        $userInfo = $reqData['data'];
        $userInfo = json_decode($userInfo, true);
        $this->userName = $userInfo['name'];
        $this->userCode = $userInfo['code'];
        $this->companyCode = $userInfo['company_code'];
        $this->appCode = $userInfo['app_code'];

        //用户角色
        if(!empty($userInfo['role'])){
            foreach($userInfo['role'] as $role){
                $this->roleCodes[] = $role['code'];
            }
        }
        //用于部门
        if(!empty($userInfo['org'])){
            foreach($userInfo['org'] as $org){
                $this->orgCodes[] = $org['code'];
            }
        }
        session('userInfo', $reqData['data']);
        //用户手动登录后跳转到首页
        //有ticket说明是单点登录跳转过来的进入初始页面
        if(!empty( $this->request->param('ticket'))){
            $this->redirect('http://cms.feiyu.com/admin/index');
        }
    }

    //验证权限
    public function checkAuth(){
        $CONTROLLER_NAME = Request()->controller();
        //首页不判断权限
        if($CONTROLLER_NAME == "Index"){
            return;
        }
        $ACTION_NAME = Request()->action();
        $ticket = cookie('sso_ticket');
        $controller = $CONTROLLER_NAME;
        $action = $ACTION_NAME;

        $apiUrl = config('AUTH_API');
        $apiUrl .= 'ApiAuth/checkAuth';
        $ssoConfig = config('SSO');
        $param = array();
        $param['ticket'] = $ticket;
        $param['controller'] = $controller;
        $param['method'] = $action;
        $param['app'] = $ssoConfig['APP'];
        $reqData = c_get($apiUrl, $param);
        if($reqData['code'] !=1){
            $this->error('对不起你没有权限');
        }
    }
}
 