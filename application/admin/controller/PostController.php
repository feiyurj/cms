<?php
namespace app\admin\controller;


use think\Controller;

use app\admin\model\PostModel;

use app\admin\model\CategoryModel;

use think\Request;


class PostController extends CommonController
{
    protected $model;
    protected $categoryModel;
    //前置操作

    public function initialize()
    {
        //parent::initialize();
        $this->model = new PostModel();
        $this->categoryModel = new CategoryModel();
    }

    public function index ()
    {
        $list = $this->model->order('sort desc')->paginate($this->pageSize);
        $cateogrys = $this->categoryModel->select();
        $this->assign('categorys', $cateogrys);
        $this->assign('list', $list);
        return $this->fetch();
    }

    public function add(){
        $categoryList = $this->categoryModel->select();
        $this->assign('categoryList', $categoryList);
        return $this->fetch('edit');
    }

    public function edit()
    {
        $code = $this->getParam('code');
        $data = $this->model->where('code',$code)->find();
        if (empty($data)){
            $this->error('code值不对，找不到相应文章');
        }
        $this->assign('data', $data);
        $categoryList = $this->categoryModel->select();
        $this->assign('categoryList', $categoryList);
        return $this->fetch('edit');
    }


    public function ajax_save ()
    {
        $data['category_code'] = $this->getParam('category');
        $data['title'] = $this->getParam('title');
        $data['summary'] = $this->getParam('summary');
        $data['key_words'] = $this->getParam('key_words');

        $data['content'] = $this->getParam('content');
        $data['start_time'] = $this->getParam('start_time');
        $data['end_time'] = $this->getParam('end_time', false);
        $data['sort'] = $this->getParam('sort');

        $data['sort'] = $this->getParam('sort');
        $data['is_top'] = $this->getParam('is_top', false);
        $data['status'] = $this->getParam('status');
        $time = time();
        $date = date('Y-m-d H:i:s');
        $data['create_time'] = $date;

        $code = $this->getParam('code', false);
        if(!empty($code)){
            $where = array();
            $where['code'] = $code;
            $result = $this->model->save($data, $where);
        }else{
            $data['code'] = md5($time);
            $result = $this->model->save($data);
        }

        if($result>0){
            ajaxSuccess('保存成功!');
        }else{
            ajaxError('保存失败!');
        }
    }

    public function upload ()
    {
        $files  = request()->file();
        $path = 'static/uploads';
        $result = array('errno'=>0, 'data'=>array());
        foreach($files as $file){
            // 移动到框架应用根目录/uploads/ 目录下
            $info = $file->move($path);
            if($info){
                // 成功上传后 获取上传信息
                //获取文件（文件名），$info->getFilename()
                //获取文件（日期/文件名），$info->getSaveName()
                $result['data'][] = '/public/'.$path.'/'.$info->getSaveName();
                echo json_encode($result);
                exit;
            }else{
                // 上传失败获取错误信息
                echo $file->getError();
            }    
        }
    }
}
