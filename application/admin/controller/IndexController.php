<?php
namespace app\admin\controller;
use think\Controller;
class IndexController extends CommonController
{
    protected $model;

    public function index()
    {   
        $this->assign('menuCategoryList', array());
        return $this->fetch();
    }
    public function test($name='name')
    {
        return 'Admin test'.$name;
    }
}
 