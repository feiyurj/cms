<?php
namespace app\admin\controller;
use think\console\Command;
use think\Controller;

class CommonController extends Controller
{
    public $pageSize = 20;

    /**
     * 封装获取参数方法
     * @param $param string 需要获取的参数
     * @param $mast boolean 是否是必须录入
     * @param $method string 请求类型 post/get
     * @return string
    */
    public function getParam($param, $mast = true, $method =''){
        $result = $this->request->param($param);
        if(empty($result) && $mast){
            $msg = '参数 ['.$param.']为空';
            $this->setError($msg);
        }
        $requestMethod = $this->request->method();
        if(!empty($method) && $requestMethod != $method){
            $msg = '参数 ['.$param.']请求方式不正确';
            $this->setError($msg);
        }
        return $result;
    }

    public function setError($msg){
        if($this->request->isAjax()){
            ajaxError($msg);
        }else{
            $this->error($msg);
        }
    }
}