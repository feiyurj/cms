/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : cms

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-07-07 16:20:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cms_category`
-- ----------------------------
DROP TABLE IF EXISTS `cms_category`;
CREATE TABLE `cms_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL DEFAULT '' COMMENT '类别编号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `opt_user` varchar(50) NOT NULL DEFAULT '' COMMENT '操作人',
  `opt_user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '操作人',
  `opt_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cms_category
-- ----------------------------

-- ----------------------------
-- Table structure for `cms_nav`
-- ----------------------------
DROP TABLE IF EXISTS `cms_nav`;
CREATE TABLE `cms_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '导航标题',
  `group` varchar(50) NOT NULL DEFAULT '' COMMENT '导航分组',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `code` varchar(50) NOT NULL DEFAULT '' COMMENT '导航编号',
  `parent_code` varchar(50) NOT NULL DEFAULT '' COMMENT '父级导航编号',
  `target` varchar(20) NOT NULL DEFAULT '' COMMENT '链接跳转方式',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序值越大越靠前',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否停用,0停用',
  `list_template` varchar(50) NOT NULL DEFAULT '' COMMENT '列表页模板名称',
  `detail_template` varchar(50) NOT NULL DEFAULT '' COMMENT '详情页模板',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cms_nav
-- ----------------------------

-- ----------------------------
-- Table structure for `cms_post`
-- ----------------------------
DROP TABLE IF EXISTS `cms_post`;
CREATE TABLE `cms_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL DEFAULT '' COMMENT '编号',
  `category_code` varchar(1000) NOT NULL DEFAULT '' COMMENT '文章类别编号,多个用逗号隔开',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '文章标题',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `key_words` varchar(200) NOT NULL DEFAULT '' COMMENT '关键字',
  `content` text NOT NULL COMMENT '文章内容',
  `view_count` int(11) NOT NULL DEFAULT '0' COMMENT '阅读数量',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间,文章开始展示时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '文章结束展示时间',
  `cover_code` varchar(100) NOT NULL DEFAULT '' COMMENT '封面图片编号',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序顺序，越大越靠前',
  `is_top` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否置顶1置顶，0不置顶',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否停用，0停用',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of cms_post
-- ----------------------------

-- ----------------------------
-- Table structure for `cms_slider`
-- ----------------------------
DROP TABLE IF EXISTS `cms_slider`;
CREATE TABLE `cms_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `cover` varchar(255) NOT NULL DEFAULT '' COMMENT '封面图片地址',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `target` varchar(255) NOT NULL DEFAULT '' COMMENT '连接打开方式_blank,_self,_parent,_top',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序，数字越大越靠前',
  `category_code` varchar(100) NOT NULL DEFAULT '' COMMENT '幻灯片类型编号',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态，1正常，2停用',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始展示时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束展示时间',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更改时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='轮播';

-- ----------------------------
-- Records of cms_slider
-- ----------------------------
